#ifndef POSTSCORE_H
#define POSTSCORE_H

#define AS_STRING(X) #X
#define VERSION_STRING(X) AS_STRING(X)

void postscore_put(const char *username, float tim);

const char *postscore_get(void);

#endif
