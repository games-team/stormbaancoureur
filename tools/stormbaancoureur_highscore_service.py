#!/usr/bin/python

# Stormbaan Coureur highscore service
# UDP based
# (c)2006-2008 by Bram Stolk
# Licensed under GPL

import socket
import string
import signal
import errno
import sys


signallist = [signal.SIGHUP,signal.SIGTERM,signal.SIGINT,signal.SIGQUIT]


class Service :

  def __init__(self) :

    self.numscores = 92
    self.filename = "./leaderboard.txt"

    # We either read the table from file, or failing that
    # we initialize the table anew.
    if not self.read_table() :
      self.init_table()

    # Start listening for datagrams (not connections!)
    self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    self.sock.bind(("", 7460))


  def __del__(self) :

    self.sock.close()


  def init_table(self) :

    self.scores = [ ("noplayer", 999.99) for i in range(self.numscores) ]


  def write_table(self) :

    f = open(self.filename, "w")
    assert f
    f.write(self.table_string())
    f.close()
    print "scoreboard has been written to file"


  def read_table(self) :

    f = open(self.filename, "r")
    if not f :
      return False
    lines = f.readlines()
    if len(lines) != self.numscores :
      return False
    self.scores = [ (string.strip(line[:8]), float(line[9:])) for line in lines ]
    print "scoreboard has been read from file"
    return True


  def table_string(self) :

    retval=""
    for s in self.scores :
      n = "%-9s"  % (s[0],)
      t = "%6.2f" % (s[1],)
      retval = retval + n + t + "\n"
    return retval


  def get_player_idx(self, name) :

    for i in range (self.numscores) :
      if self.scores[i][0] == name :
        return i
    return -1


  def handle_score(self, data) :

    n = string.strip(data[:8]).replace(",",".")
    t = float(data[9:15])
    v = data[16:]
    if t <= 0 :
      return True

    # Is it a personal best?
    # if not, do not record it.
    previdx = self.get_player_idx(n)
    if previdx != -1 :
      if self.scores[previdx][1] <= t :
        return True
      else :
        # personal best: remove old one
        self.scores.pop(previdx)

    idx=0
    while idx<len(self.scores) and self.scores[idx][1] < t :
      idx+=1
    if idx<self.numscores :
      self.scores.insert(idx, (n,t))
      if len(self.scores) > self.numscores :
        self.scores.pop()
    return True


  def sustain(self) :

    while True :
      try :
        (data,addr) = self.sock.recvfrom(1472)
        print "got",data,"from", addr
        ok = self.handle_score(data)
        if ok :
          self.sock.sendto(self.table_string(), 0, addr)
      except socket.error, (e, msg) :
        print e, msg
        if e == errno.EAGAIN :
          return
        else :
          self.alive = False



def signal_hup_handler(signum, frame) :

  global s
  # reinstate old signalhandlers
  for i in signallist :
    signal.signal(i, signal.SIG_DFL)
  s.write_table()
  del s
  sys.exit(0)


if __name__ == '__main__' :

  global s
  s = Service()
  for i in signallist :
    signal.signal(i, signal_hup_handler)
  s.sustain()


